﻿using System;
using System.IO;
using CurrencyConversion;
using System.Collections.Generic;
using System.Text;

namespace LuccaHelper
{
    class Loader
    {
        private String pathFile;

        public Loader(String pathFile_)
        {
            pathFile = pathFile_;
        }

        public bool loadData(ref CurrencyConverter currencyConverter, ref string error)
        {
            if (!File.Exists(pathFile))
            {
                error = "Specified file doesn't exist";
                return false;
            }

            var list = new List<string>();
            var fileStream = new FileStream(pathFile, FileMode.Open, FileAccess.Read);
            using (var streamReader = new StreamReader(fileStream, Encoding.UTF8))
            {
                string firstLine = "";
                int lineNb = 0;
                int totConvLines = 0;
                string line;

                while ((line = streamReader.ReadLine()) != null)
                {
                    if (lineNb == 0)
                        firstLine = line;
                    else if (lineNb == 1)
                    {
                        string[] dataFirstLine = firstLine.Split(';');
                        if (dataFirstLine.Length < 3)
                        {
                            error = "File doesn't contain all needed information!. First line miss some arguments.";
                            return false;
                        }

                        if (dataFirstLine[0].Length != 3 || dataFirstLine[2].Length != 3)
                        {
                            error = "Currency need to be specified in 3 chars. Please check first line";
                            return false;
                        }

                        bool res = int.TryParse(line, out totConvLines);
                        if (res == false)
                        {
                            error = "Line two is not an integer!";
                            return false;
                        }

                        currencyConverter = new CurrencyConverter(totConvLines);
                        currencyConverter.CurrencyFrom = dataFirstLine[0];
                        currencyConverter.CurrencyTo = dataFirstLine[2];

                        int amount = 0;
                        res = int.TryParse(dataFirstLine[1], out amount);
                        if (res == false)
                        {
                            error = "Amount specified in the first line is not an integer!";
                            return false;
                        }

                        if (amount <= 0)
                        {
                            error = "Amount specified in the first line is not positive!";
                            return false;
                        }

                        currencyConverter.Amount = amount;
                    }
                    else
                    {
                        string[] dataLine = line.Split(';');
                        if (dataLine.Length < 3)
                        {
                            error = "File is not valid. Line " + (lineNb + 1) + " doesn't follow the convention DD;DA;T.";
                            return false;
                        }

                        if (dataLine[0].Length != 3 || dataLine[1].Length != 3)
                        {
                            error = "Currency need to be specified in 3 chars. Please check line " + (lineNb + 1);
                            return false;
                        }

                        double conversionFactor;
                        string[] doubleAsStr = dataLine[2].Split('.');
                        if (doubleAsStr.Length != 2)
                        {
                            error = "Conversion factor specified at line " + (lineNb + 1) + " is not a number with decimals!";
                            return false;
                        }

                        if (doubleAsStr[1].Length != 4)
                        {
                            error = "Conversion factor specified at line " + (lineNb + 1) + " is not a number with 4 decimals!";
                            return false;
                        }

                        bool res = double.TryParse(dataLine[2].Replace('.', ','), out conversionFactor);
                        if (res == false)
                        {
                            error = "Conversion factor specified at line " + (lineNb + 1) + " is not a number!";
                            return false;
                        }

                        currencyConverter.addCurrencyConversion(dataLine[0].Trim(), dataLine[1].Trim(), conversionFactor);
                    }
                    lineNb++;
                }

                if (lineNb < 3 || lineNb != totConvLines + 2)
                {
                    error = "File format is not correct!";
                    return false;
                }
            }

            return true;
        }
    }

}