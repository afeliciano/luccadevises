﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CurrencyConversion
{
    class CurrencyConversion
    {
        private String currencyFrom;
        private String currencyTo;
        private double conversionFactor;

        public CurrencyConversion(String currencyFrom_, String currencyTo_, double conversionFactor_)
        {
            currencyFrom = currencyFrom_;
            currencyTo = currencyTo_;
            conversionFactor = conversionFactor_;
        }

        public String CurrencyFrom
        {
            get
            {
                return this.currencyFrom;
            }
            set
            {
                this.currencyFrom = value;
            }
        }

        public String CurrencyTo
        {
            get
            {
                return this.currencyTo;
            }
            set
            {
                this.currencyTo = value;
            }
        }

        public double ConvertionFactor
        {
            get
            {
                return this.conversionFactor;
            }
            set
            {
                this.conversionFactor = value;
            }
        }

    }

    class CurrencyConverter
    {
        private List<CurrencyConversion> listConversion;
        private int amount;
        private String currencyFrom;
        private String currencyTo;

        public CurrencyConverter(int listSize)
        {
            listConversion = new List<CurrencyConversion>(listSize);
        }

        public String CurrencyFrom
        {
            get
            {
                return this.currencyFrom;
            }
            set
            {
                this.currencyFrom = value;
            }
        }

        public String CurrencyTo
        {
            get
            {
                return this.currencyTo;
            }
            set
            {
                this.currencyTo = value;
            }
        }

        public int Amount
        {
            get
            {
                return this.amount;
            }
            set
            {
                this.amount = value;
            }
        }

        public void addCurrencyConversion(String currencyFrom, String currencyTo, double conversionFactor)
        {
            listConversion.Add(new CurrencyConversion(currencyFrom, currencyTo, conversionFactor));
        }

        public bool performeConversion(ref int amountConverted, ref String error, bool verbose = false)
        {
            LuccaGraph.Graph<int> graph = new LuccaGraph.Graph<int>();
            LuccaGraph.Graph<double> graphConv = new LuccaGraph.Graph<double>();

            foreach (CurrencyConversion currencyConvertion in listConversion)
            {
                graph.addEdgeToVertex(currencyConvertion.CurrencyFrom, currencyConvertion.CurrencyTo, 1);
                graph.addEdgeToVertex(currencyConvertion.CurrencyTo, currencyConvertion.CurrencyFrom, 1);

                graphConv.addEdgeToVertex(currencyConvertion.CurrencyFrom, currencyConvertion.CurrencyTo, currencyConvertion.ConvertionFactor);
                graphConv.addEdgeToVertex(currencyConvertion.CurrencyTo, currencyConvertion.CurrencyFrom, Math.Round(1 / currencyConvertion.ConvertionFactor, 4));
            }

            List<String> path = LuccaGraph.Dijkstras.shortestPath(currencyFrom, currencyTo, graph, verbose);
            if (path == null)
            {
                error = "Conversion is not possible due to missing path from " + currencyFrom + " to " + currencyTo;
                return false;
            }

            double amount = Amount;
            for (int i = 0; i < path.Count() - 1; ++i)
                amount = Math.Round(amount * graphConv.getGraph()[path[i]][path[i + 1]], 4);

            amountConverted = (int) Math.Round(amount, 0);
            return true;
        }

    }
}
