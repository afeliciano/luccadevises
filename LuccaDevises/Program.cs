﻿using System;

namespace CurrencyConversion
{
    class Program
    {
        static void Main(string[] args)
        {
            bool verbose = false;
            if (args.Length == 0)
            {
                System.Console.WriteLine("Please enter the path to file as parameter.");
                return;
            }
                        
            if (args.Length == 2)
            {
                verbose = String.Compare(args[1], "-v") == 0;
            }

            LuccaHelper.Loader loader = new LuccaHelper.Loader(args[0]);
            String error = "";
            CurrencyConverter currencyConverter = null;
            if (loader.loadData(ref currencyConverter, ref error) && currencyConverter != null)
            {
                int amountConverted = 0;
                if (currencyConverter.performeConversion(ref amountConverted, ref error, verbose))
                    Console.WriteLine(amountConverted);
                else
                    Console.WriteLine("Cannot execute the program. Error : '" + error + "'");
            }
            else
                Console.WriteLine("Cannot execute the program. Error : '" + error + "'");
        }
    }
}
