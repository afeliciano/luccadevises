﻿using System;
using System.Collections.Generic;

namespace LuccaGraph
{
    class Graph<T>
    {
        private Dictionary<String, Dictionary<String, T>> vertices = new Dictionary<String, Dictionary<String, T>>();

        public void addVertex(String name, Dictionary<String, T> edges)
        {
            vertices[name] = edges;
        }

        public void addEdgeToVertex(String fromVertex, String toVertex, T weight)
        {
            if (!vertices.ContainsKey(fromVertex))
                vertices[fromVertex] = new Dictionary<String, T>();

            if (vertices[fromVertex].ContainsKey(toVertex))
                vertices[fromVertex][toVertex] = weight;
            else
                vertices[fromVertex].Add(toVertex, weight);
        }

        public Dictionary<String, Dictionary<String, T>> getGraph()
        {
            return vertices;
        }
    }

    class Dijkstras
    {
        public static List<String> shortestPath(String start, String finish, Graph<int> graph, bool showPath = false)
        {
            var previous = new Dictionary<String, String>();
            var distances = new Dictionary<String, int>();
            var nodes = new List<String>();

            List<String> path = null;

            foreach (var vertex in graph.getGraph())
            {
                if (vertex.Key == start)
                {
                    distances[vertex.Key] = 0;
                }
                else
                {
                    distances[vertex.Key] = int.MaxValue;
                }

                nodes.Add(vertex.Key);
            }

            while (nodes.Count != 0)
            {
                nodes.Sort((x, y) => distances[x] - distances[y]);

                var smallest = nodes[0];
                nodes.Remove(smallest);

                if (String.Compare(smallest, finish) == 0)
                {
                    path = new List<String>();
                    while (previous.ContainsKey(smallest))
                    {
                        path.Add(smallest);
                        smallest = previous[smallest];
                    }

                    break;
                }

                if (distances[smallest] == int.MaxValue)
                {
                    break;
                }

                foreach (var neighbor in graph.getGraph()[smallest])
                {
                    var alt = distances[smallest] + neighbor.Value;
                    if (alt < distances[neighbor.Key])
                    {
                        distances[neighbor.Key] = alt;
                        previous[neighbor.Key] = smallest;
                    }
                }
            }

            if (path != null)
            {
                path.Add(start);
                path.Reverse();

                if (showPath)
                {
                    String pathTaken = "Path taken is : ";
                    for (int i = 0; i < path.Count; ++i)
                    {
                        pathTaken += path[i] + " ";
                        if (i != path.Count - 1)
                            pathTaken += "=> ";
                    }

                    Console.WriteLine(pathTaken);
                }
            }           
            
            return path;
        }
    }
}

